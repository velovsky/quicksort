var manager = 
{
	queue: [],
	new: function()
	{
		manager.queue = [];
	},
	enqueue: function()
	{
		var value = parseInt($("#input_field")[0].value);
		manager.queue.unshift(value);
		manager.output();
	},
	dequeue: function()
	{
		if(manager.queue.length < 1)
			return;
		
		manager.queue.splice(manager.queue.length-1,1);
		manager.output();
	},
	front: function()
	{
		if(manager.queue.length < 1)
			return;
		
		$("#front")[0].innerHTML = manager.queue[manager.queue.length-1];
	},
	rear: function()
	{
		if(manager.queue.length < 1)
			return;
		
		$("#rear")[0].innerHTML = manager.queue[0];
	},
	output: function()
	{		
		var output = "["
		output += manager.queue.toString();
		output += "]";
		
		//set output
		$("#output_field")[0].innerHTML = output;
	}
}