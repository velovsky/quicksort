var quicksort =
{
	array: [],
	pivot_index: undefined,
	low_index: 0,
	high_index: undefined,
	main: function()
	{		
		//array
		quicksort.array = manager.queue;

		if(quicksort.array.length <= 1)
			return;

		//indexes
		quicksort.low_index = 0;
		quicksort.high_index = quicksort.array.length - 1;
		
		//run quicksort
		quicksort.sort(quicksort.low_index,quicksort.high_index)
		
		manager.output();
	},
	sort: function(low_index,high_index)
	{
		
		if(low_index < high_index)
		{
			//partition
			quicksort.pivot_index = quicksort.partition(low_index,high_index);
			
			quicksort.sort(low_index,quicksort.pivot_index-1);
			quicksort.sort(quicksort.pivot_index+1,high_index);
		}
		
	},
	partition: function(low,high)
	{
		var pivot = quicksort.array[high]; //highest index
		
		var i = low - 1;
		
		for(var j = low; j <= high - 1; j++)
		{
			if(quicksort.array[j] <= pivot)
			{
				i++;
				//swap
				var tempj = quicksort.array[j];
				quicksort.array[j] = quicksort.array[i];
				quicksort.array[i] = tempj;
			}
		}
		//swap
		var tempj = quicksort.array[high];
		quicksort.array[high] = quicksort.array[i+1];
		quicksort.array[i+1] = tempj;
		
		return i+1;
	}
	
}